package product_categories
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import commons.ResponseData
import commons.CommonStep
import commons.VerifyError


class delete_category {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def data = new ResponseData()
	def slurper = new groovy.json.JsonSlurper()
	def deletedId = 0

	@Given("I have category to be deleted")
	def I_have_category_to_be_deleted() {
		def response = CommonStep.postDefaultCategory()
		def json = slurper.parseText(response.getResponseBodyContent())
		deletedId = json.data.ID
	}

	@When("I delete the given category")
	def I_delete_the_given_category() {
		def response = WebUI.callTestCase(
				findTestCase('_pages/product_categories/delete_category'),
				[('id'): deletedId],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@When("I delete a given category with ID (.*)")
	def I_delete_a_given_category_with_id(String ID) {
		int id = ID as int
		def response = WebUI.callTestCase(
				findTestCase('_pages/product_categories/delete_category'),
				[('id'): id],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify the successful deletion response")
	def I_verify_deletion_response_with_status() {
		def response = data.getResponse()
		WebUI.callTestCase(
				findTestCase('_pages/product_categories/assert_delete_category'),
				[('response'):response],
				FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify if category deletion returns error")
	def I_verify_if_category_deletion_returns_error() {
		VerifyError.verify(data.getResponse())
	}
}