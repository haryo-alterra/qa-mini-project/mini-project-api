#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@products
Feature: Products
  I want to be able to manage available products

  @get_all_products
  Scenario: I want to check all available products
  	When I retrieve all products
  	Then I make sure every product is of valid format
  	
  @post_product
  Scenario Outline: I want to be able to post new product
  	Given I have a newly created category for my new product
  	When I add product with name <name>, description <description>, and price <price>
  	Then I verify successful product post request
  	
  	Examples:
  	| name | description | price |
  	| Produk asal2an | deskripsi asal2an | 10000 |
  	| Produk asal2an | | 10000 |
  	
  @post_product_empty_name
  Scenario Outline: I should not be able to post new product with empty name
  	Given I have a newly created category for my new product
  	When I add product with name <name>, description <description>, and price <price>
  	Then I verify the product post request error
  	
  	Examples:
  	| name | description | price |
  	| | deskripsi asal2an | 10000 |
  	| | cek123 | 1000000 |
  	
  @post_product_multiple_category
  Scenario: I want to be able to post new product and assign it to multiple newly created categories
  	Given I have 5 different categories for my new product
  	When I add product with name PS3, description deskripsi, and price 250000
  	Then I verify successful product post request
  	
  @post_product_empty_category
  Scenario: I want to be able to post new product even without any category assigned
  	When I add product with name PS3, description deskripsi, and price 250000
  	Then I verify successful product post request
  	
  @post_product_invalid_category
  Scenario: I want to be able to post new product even if the category cannot be found
  	When I add product with name PS3, description deskripsi, and price 250000
  	And I have category ID 2147483647
  	Then I verify successful product post request
  	
  @get_product
  Scenario: I want to be able to retrieve details about my created product
  	Given I have a product I want to check
  	When I retrieve the given product
  	Then I verify the successful product retrieval response
  	  	
  @get_invalid_product
  Scenario: I should not be able to retrieve non-existing product data
  	When I retrieve a given product with ID 2147483647
  	Then I verify the product retrieval error
  	
  @delete_product
  Scenario: I want to delete my product
   	Given I have a product I want to delete
   	When I delete the given product
   	Then I verify the successful product deletion response
   	
  @delete_invalid_product
  Scenario: I should not be able to delete non-existing product data
  	When I delete a given product with ID 2147483647
  	Then I verify the product deletion error
  	
  @rating_product
  Scenario Outline: I want to rate my product
  	Given I have a product I want to rate
  	When I rate the given product with score <score>
  	Then I verify the successful product rating post response
  	
  	Examples:
  	| score |
  	| 4 |
  	| 0 |
  	
  @rating_product_invalid_score
  Scenario Outline: I should not be able to rate product with invalid score 
  	Given I have a product I want to rate
  	When I rate the given product with score <score>
  	Then I verify product rating error
  	
  	Examples:
  	| score |
  	| 10 |
  	| 6 |
  
  @rating_invalid_product
  Scenario: I should not be able to rate a non-existing product data
  	When I rate product with ID 2147483647 with score 4
  	Then I verify product rating error
  	
  @get_product_rating
  Scenario: I want to get a specific product's rating
  	Given I have a product I want to check its rating
  	When I check the given product rating
  	Then I verify the successful product rating response
  	
 	@get_invalid_product_rating
 	Scenario: I should not be able to get rating from non-existent product
 		When I check the rating of a product with ID 2147483647
 		Then I verify the product rating retrieval error
 	
 	@post_product_comment
 	Scenario: I want to be able to post comment on a product
 		Given I want to comment on my newly-created product
 		When I comment on the given product with comment komen asal
 		Then I verify if my comment is sent
 		
 	@post_product_empty_comment
 	Scenario: I should not be able to post empty comment
 		Given I want to comment on my newly-created product
 		When I comment on the given product with empty comment
 		Then I verify if my comment post process results in error
 		
 	@get_product_comment
 	Scenario: I want to retrieve comment from a product
 		Given I have a product with random comment
 		When I check that product for comment
 		Then I verify if the comment is retrieved