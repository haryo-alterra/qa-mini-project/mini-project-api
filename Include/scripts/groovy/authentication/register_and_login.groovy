package authentication
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import commons.Randomizer
import commons.ResponseData

class register_and_login {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def data = new ResponseData()
	def slurper = new groovy.json.JsonSlurper()
	def email = ""
	def pass = ""

	@When("I register with a random email and a given password (.*)")
	def I_register_with_random_email_and_given_password(String password) {
		String randomizedEmail = Randomizer.randomizeString(8)
		randomizedEmail += "@gmail.com"
		WebUI.callTestCase(
				findTestCase('_pages/authentication/register/register'),
				[('email'):randomizedEmail, ('password'):password],
				FailureHandling.STOP_ON_FAILURE)
		email = randomizedEmail
		pass = password
	}

	@And("I log in with same credential")
	def I_log_in_with_same_email_and_given_password() {
		def response = WebUI.callTestCase(
				findTestCase('_pages/authentication/login/login'),
				[('email'):email, ('password'):pass],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I check if login is successful and returned token is valid")
	def I_check_if_login_successful_and_token_valid() {
		def response = data.getResponse()
		WebUI.callTestCase(
				findTestCase('_pages/authentication/login/assert_login'),
				[('response'):response, ('email'):email],
				FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify if my user information returns correct info")
	def I_verify_if_my_user_information_returns_correct_info() {
		def response = data.getResponse()
		def json = slurper.parseText(response.getResponseBodyContent())
		String jwt = json.data

		def userInfoResponse = WebUI.callTestCase(
				findTestCase('_pages/authentication/user_information/get_user_information'),
				[('token'): jwt], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(
				findTestCase('_pages/authentication/user_information/assert_get_user_information'),
				[('response'): userInfoResponse, ('email'): email],
				FailureHandling.STOP_ON_FAILURE)
	}
}