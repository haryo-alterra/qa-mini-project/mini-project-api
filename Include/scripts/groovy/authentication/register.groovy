package authentication
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.util.logging.Commons

import commons.ResponseData
import commons.Randomizer
import commons.VerifyError

class register {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def data = new ResponseData()

	@When("I register with random valid email address and password (.*)")
	def I_register_with_random_valid_email_address(String password) {
		String email = Randomizer.randomizeString(8)
		email += "@gmail.com"
		def response = WebUI.callTestCase(
				findTestCase('_pages/authentication/register/register'),
				[('email'):email, ('password'):password],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@When("I register with random invalid email address and password (.*)")
	def I_register_with_random_invalid_email_address(String password) {
		String email = Randomizer.randomizeString(8)
		def response = WebUI.callTestCase(
				findTestCase('_pages/authentication/register/register'),
				[('email'):email, ('password'):password],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@When("I register twice with same email with password (.*)")
	def I_register_twice_with_same_email(String password) {
		def randomizedEmail = Randomizer.randomizeString(8) + "@gmail.com"
		WebUI.callTestCase(
				findTestCase('_pages/authentication/register/register'),
				[('email'):randomizedEmail, ('password'):password])
		def secondResponse = WebUI.callTestCase(
				findTestCase('_pages/authentication/register/register'),
				[('email'):randomizedEmail, ('password'):password])
		secondResponse.getClass()
		data.setResponse(secondResponse)
	}

	@Then("I verify the successful resulting register response")
	def I_verify_the_register_response() {
		def response = data.getResponse()
		WebUI.callTestCase(
				findTestCase('_pages/authentication/register/assert_register'),
				[('response'):response],
				FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify if register attempt returns error")
	def I_verify_register_attempt_returns_error() {
		VerifyError.verify(data.getResponse())
	}
}