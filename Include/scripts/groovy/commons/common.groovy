package commons
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import commons.TestObjectAttributes


class CommonStep {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def static slurper = new groovy.json.JsonSlurper()

	public static ResponseObject postDefaultCategory() {
		Map<String, Object> defaultMap = TestObjectAttributes.map(
				'Postman/Product Categories/Create a category')
		String defaultName = defaultMap.get("name")
		String defaultDesc = defaultMap.get("description")
		ResponseObject response = WebUI.callTestCase(
				findTestCase('_pages/product_categories/post_category'),
				[('name'): defaultName, ('description'): defaultDesc],
				FailureHandling.STOP_ON_FAILURE)
		return response
	}

	public static ResponseObject postDefaultProduct() {
		def categoryResponse = postDefaultCategory()
		def json = slurper.parseText(categoryResponse.getResponseBodyContent())
		int retrievedId = json.data.ID
		Map<String, Object> defaultMap = TestObjectAttributes.map(
				'Postman/Products/Create a new product')
		String defaultName = defaultMap.get("name")
		String defaultDesc = defaultMap.get("description")
		int defaultPrice = defaultMap.get("price")
		List<Integer> ids = new ArrayList<>()
		ids.add(retrievedId)

		ResponseObject response = WebUI.callTestCase(
				findTestCase('_pages/products/products/post_product'),
				[
					('name'): defaultName,
					('description'): defaultDesc,
					('price'): defaultPrice,
					('categories'): ids
				],
				FailureHandling.STOP_ON_FAILURE)
		return response
	}

	public static ResponseObject postDefaultProductWithComment() {
		def productResponse = postDefaultProduct()
		def json = slurper.parseText(productResponse.getResponseBodyContent())
		int id = json.data.ID
		Map<String, Object> defaultMap = TestObjectAttributes.map(
				'Postman/Products/Create a comment for product')
		String defaultComment = defaultMap.get("comment")

		ResponseObject response = WebUI.callTestCase(
				findTestCase('_pages/products/comments/post_product_comment'),
				[
					('id'): id,
					('comment'): defaultComment,
				],
				FailureHandling.STOP_ON_FAILURE)
		return productResponse
	}
}