<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Create a new product</name>
   <tag></tag>
   <elementGuidId>81af606d-3e48-4031-9af6-49aac000f3ce</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;name\&quot;:\&quot;${name}\&quot;,\n    \&quot;description\&quot;:\&quot;${description}\&quot;,\n    \&quot;price\&quot;:${price},\n    \&quot;categories\&quot;:${categories}\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>aea26056-8d03-4a9e-b47c-2385304fd619</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${bearer}</value>
      <webElementGuid>e736e813-2d74-45c0-8a52-8b2141e6798c</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${BASE_URL}products</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.BASE_URL</defaultValue>
      <description></description>
      <id>20c0a540-ffae-4f0e-9d55-2eec11ac91c7</id>
      <masked>false</masked>
      <name>BASE_URL</name>
   </variables>
   <variables>
      <defaultValue>[1]</defaultValue>
      <description></description>
      <id>a9d4fe16-abd2-4277-af1c-ae1f7c21f2c3</id>
      <masked>false</masked>
      <name>categories</name>
   </variables>
   <variables>
      <defaultValue>'Sony PS5'</defaultValue>
      <description></description>
      <id>81378005-d5d3-434c-b156-f9bbb1134624</id>
      <masked>false</masked>
      <name>name</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>40702921-9796-4d4b-be41-8a187a40b2bd</id>
      <masked>false</masked>
      <name>description</name>
   </variables>
   <variables>
      <defaultValue>299</defaultValue>
      <description></description>
      <id>0ca3816a-e845-4928-a646-ff8a8788ae76</id>
      <masked>false</masked>
      <name>price</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.user_token</defaultValue>
      <description></description>
      <id>4f7f11d1-5b4b-4774-a28d-b6ba717edee5</id>
      <masked>false</masked>
      <name>token</name>
   </variables>
   <variables>
      <defaultValue>'Bearer ${token}'</defaultValue>
      <description></description>
      <id>880f875e-a09f-4e8c-af7f-24467bdc7fad</id>
      <masked>false</masked>
      <name>bearer</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
