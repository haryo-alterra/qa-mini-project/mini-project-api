package products
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import commons.ResponseData
import commons.CommonStep
import commons.VerifyError

class post_product {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def data = new ResponseData()
	def slurper = new groovy.json.JsonSlurper()

	String name = ""
	String description = ""
	int price = 0
	List<Integer> categoryIds = new ArrayList<>();

	@Given("I have a newly created category for my new product")
	def I_have_new_category_for_new_product() {
		def response = CommonStep.postDefaultCategory()
		def json = slurper.parseText(response.getResponseBodyContent())
		categoryIds.add(json.data.ID)
	}

	@Given("I have (\\d+) different categories for my new product")
	def I_have_different_categories_for_my_new_products(int num) {
		for (int i = 0; i < num; i++) {
			def response = CommonStep.postDefaultCategory()
			def json = slurper.parseText(response.getResponseBodyContent())
			categoryIds.add(json.data.ID)
		}
	}

	@When("I add product with name (.*), description (.*), and price (\\d+)")
	def I_add_product_with_given_name_desc_and_price(String name, String description, int price) {
		def response = WebUI.callTestCase(
				findTestCase('_pages/products/products/post_product'),
				[('name'): name, ('description'): description, ('price'): price, ('categories'): categoryIds],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@And("I have category ID (\\d+)")
	def I_have_category_ID(int id) {
		categoryIds.add(id)
	}

	@Then("I verify successful product post request")
	def I_verify_successful_product_post_request() {
		def response = data.getResponse()
		WebUI.callTestCase(
				findTestCase('_pages/products/products/assert_post_product'),
				[('response'): response],
				FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify the product post request error")
	def I_verify_product_post_request_error() {
		VerifyError.verify(data.getResponse())
	}
}