#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@authentication
Feature: Authentication
  I want to check API's authentication functionality
  
  @register
  Scenario: I want to register as a new user
  	When I register with random valid email address and password haryo1234
  	Then I verify the successful resulting register response

  @invalid_email_register
  Scenario: I want to be warned if my email address is of invalid format
  	When I register with random invalid email address and password haryo1234
  	Then I verify if register attempt returns error
  	
  @same_email_register
  Scenario: I want to be warned if I want to register using already registered email
  	When I register twice with same email with password haryo1234
  	Then I verify if register attempt returns error
  	
  @register_and_login
  Scenario: I want to be able to log in my newly registered account
  	When I register with a random email and a given password abababab
  	And I log in with same credential
  	Then I check if login is successful and returned token is valid
  	
  @login
  Scenario: I want to log in to be able to use the app
  	When I log in with email someone@mail.com and password 123123
  	Then I verify if login is valid and successful
  	
  @invalid_credentials
  Scenario Outline: I should not be able to log in with incorrect login credentials
  	When I log in with email <email> and password <password>
  	Then I verify if login attempt returns error
  
  	Examples:
  	| email | password |
  	| someone@mail.com | 123 |
  	| oebvrienvcksdjfwef@mail.co.uk | 123123 |
  	|| 123123 |
  
  @get_user_information
  Scenario: I want to get information about my account
  	When I register with a random email and a given password abababab
  	And I log in with same credential
  	Then I verify if my user information returns correct info
  	
  @post_comment_empty_auth
  Scenario: I should not be able to comment without any auth token
  	Given I have a product that I want to comment without auth
  	When I comment komentar with no bearer token in header
  	Then I expect my post request would fail to send
  	
  @post_product_empty_auth
  Scenario: I should not be able to add product without any token
  	Given I have available category for my product that I can check without auth
  	When I am not logged in and I add product with name PS6, description uwogh, and price 10000000
  	Then I expect my post request would fail to send
  