package authentication
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import commons.ResponseData
import commons.CommonStep
import commons.VerifyError


class product_crud_auth {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	def data = new ResponseData()
	def slurper = new groovy.json.JsonSlurper()
	def productId = 0
	List<Integer> categoryIds = new ArrayList<>();

	@Given("I have available category for my product that I can check without auth")
	def I_have_new_category_for_new_product() {
		def response = CommonStep.postDefaultCategory()
		def json = slurper.parseText(response.getResponseBodyContent())
		categoryIds.add(json.data.ID)
	}

	@Given("I have a product that I want to comment without auth")
	def I_have_product_with_random_comment() {
		def response = CommonStep.postDefaultProduct()
		def json = slurper.parseText(response.getResponseBodyContent())
		productId = json.data.ID
	}

	@When("I comment (.*) with no bearer token in header")
	def I_comment_with_no_bearer(String comment) {
		def response = WebUI.callTestCase(
				findTestCase('_pages/products/comments/post_product_comment_auth'),
				[('id'): productId, ('comment'): comment, ('bearer'): ""],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@When("I am not logged in and I add product with name (.*), description (.*), and price (\\d+)")
	def I_am_not_logged_in_add_product_with_given_name_desc_and_price(String name, String description, int price) {
		def response = WebUI.callTestCase(
				findTestCase('_pages/products/products/post_product_auth'),
				[('name'): name,
					('description'): description,
					('price'): price,
					('categories'): categoryIds,
					('bearer'): ""],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}


	@Then("I expect my post request would fail to send")
	def I_expect_my_post_request_would_fail_to_send() {
		VerifyError.verify(data.getResponse())
	}
}