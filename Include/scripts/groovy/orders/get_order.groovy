package orders
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import commons.ResponseData
import commons.CommonStep


class get_order {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def data = new ResponseData()
	def slurper = new groovy.json.JsonSlurper()
	def orderId = 0

	@Given("I have ordered a product")
	def I_have_ordered_a_product() {
		def productResponse = CommonStep.postDefaultProduct()
		def json = slurper.parseText(productResponse.getResponseBodyContent())
		def response = WebUI.callTestCase(
				findTestCase('_pages/orders/post_order'),
				[('product_id'): json.data.ID, ('quantity'): 2],
				FailureHandling.STOP_ON_FAILURE)
		def orderJson = slurper.parseText(response.getResponseBodyContent())
		orderId = orderJson.data.get(0).ID
	}

	@When("I want to check that particular product order")
	def I_want_to_check_that_particular_product_order() {
		def response = WebUI.callTestCase(
				findTestCase('_pages/orders/get_order'),
				[('id'): orderId],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify if that product order is present")
	def I_verify_if_that_product_order_is_present() {
		def response = data.getResponse()
		WebUI.callTestCase(
			findTestCase('_pages/orders/assert_get_order'),
			[('response'): response],
			FailureHandling.STOP_ON_FAILURE
		)
	}
}