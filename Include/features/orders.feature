#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@orders
Feature: Orders Feature
  I want to be able to order from this app

  @post_order
  Scenario: I want to order a product
    Given I have a product I want to order
    When I order them in 10 amount
    Then I verify the resulting order submission
  
  @get_all_orders
  Scenario: I want to retrieve all orders I have submitted
  	When I retrieve all orders
  	Then I make sure every order is of valid format
  	
 	@get_order
 	Scenario: I want to retrieve a specific order I have done
 		Given I have ordered a product
 		When I want to check that particular product order
 		Then I verify if that product order is present