#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@categories
Feature: Categories
	I want to be able to manage product's categories
	
	@post_category
	Scenario Outline: I want to be able to post a new category
		When I add new category with name <name> and description <description>
		Then I verify the successful posted category
		
		Examples:
		| name | description |
  	| test 123 | hanya untuk testing |
  	| test 123 ||
  
  @post_category_empty_name
	Scenario Outline: I should not be able to post new category with empty name
		When I add new category with name <name> and description <description>
		Then I verify if category post returns error
		
		Examples:
		| name | description |
  	| | hanya untuk testing |
  	| | |
  	
  @delete_category
  Scenario: I want to delete newly created category
  	Given I have category to be deleted
  	When I delete the given category
  	Then I verify the successful deletion response
  
  @delete_invalid_category
  Scenario: I should not be able to delete non-existing category
  	When I delete a given category with ID 2147483647
  	Then I verify if category deletion returns error
  	
  @get_category
  Scenario: I want to retrieve detail about my newly created category
  	Given I have category to be retrieved
  	When I retrieve the given category
  	Then I verify the successful category retrieval response
  	
  @get_invalid_category
  Scenario: I should not be able to delete non-existing category
  	When I retrieve a given category with ID 2147483647
  	Then I verify if category retrieval returns error
  	
  @get_all_categories
  Scenario: I want to check all available categories
  	When I retrieve all categories
  	Then I make sure every category is of valid format