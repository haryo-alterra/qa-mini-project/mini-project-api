package products
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import commons.ResponseData
import commons.CommonStep
import commons.VerifyError

class get_product {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	def data = new ResponseData()
	def slurper = new groovy.json.JsonSlurper()
	def productId = 0

	@Given("I have a product I want to check")
	def I_have_a_product_i_want_to_check() {
		def response = CommonStep.postDefaultProduct()
		def json = slurper.parseText(response.getResponseBodyContent())
		productId = json.data.ID
	}

	@When("I retrieve the given product")
	def I_retrieve_the_given_product() {
		def response = WebUI.callTestCase(
				findTestCase('_pages/products/products/get_product'),
				[('id'): productId],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@When("I retrieve a given product with ID (.*)")
	def I_retrieve_a_given_product_with_id(String ID) {
		int id = ID as int
		def response = WebUI.callTestCase(
				findTestCase('_pages/products/products/get_product'),
				[('id'): id],
				FailureHandling.STOP_ON_FAILURE)
		response.getClass()
		data.setResponse(response)
	}

	@Then("I verify the successful product retrieval response")
	def I_verify_successful_retrieval_responses() {
		def response = data.getResponse()
		WebUI.callTestCase(
				findTestCase('_pages/products/products/assert_get_product'),
				[('response'):response],
				FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify the product retrieval error")
	def I_verify_product_retrieval_error() {
		VerifyError.verify(data.getResponse())
	}
}